Instalacja:
	poetry install

Uruchomienie:
	poetry shell
	python pygame_tile_test.py x, gdzie x to liczba między 0 a 1900

Działanie:
	Trzeba wcisnąć jakiś przycisk, żeby zobaczyć kafelki.
	Wciśnięcie c powoduje wyświetlenie indexów kafelków.
	Wciśnięcie q zamyka okno.

Adnotacje:
	Dobrym pomysłem jest używanie pyenv, wtedy można w katalogu z kodem zainstalować lokalną wersję Pythona.
	pyenv i poetry powinny być w większości dystrybucji
