from sys import argv
import pygame
import pygame.locals


def load_tile_table(filename):
    image = pygame.image.load(filename).convert()
    image_width, image_height = image.get_size()
    tile_table = []
    for tile_y in range(int(image_height / 32)):
        for tile_x in range(int(image_width / 32)):
            rect = (tile_x * 32, tile_y * 32, 32, 32)
            tile_table.append(image.subsurface(rect))
    return tile_table


if __name__ == '__main__':
    pygame.init()
    # 48 * 32, 16 * 32 = 1536, 512
    map_width = 48
    map_height = 24
    screen = pygame.display.set_mode((map_width * 32, map_height * 32))
    screen.fill((255, 255, 255))
    font = pygame.font.SysFont(pygame.font.get_default_font(), 16)
    tiles = pygame.Surface((map_width * 32, map_height * 32))
    captions = pygame.Surface((map_width * 32, map_height * 32), flags=pygame.SRCALPHA)
    captions.fill((0, 0, 0, 0))
    table = load_tile_table('tiles.png')
    offset = 0
    if len(argv) > 1:
        if argv[1].isdigit():
            offset = int(argv[1])
        if offset > 1900:
            offset = 1900
    for y in range(map_height):
        for x in range(map_width):
            i = y * map_width + x + offset
            tiles.blit(table[i], (x * 32, y * 32))
            text = font.render(str(i), True, (255, 255, 255), (0, 0, 0))
            captions.blit(text, (x * 32, y * 32))
    key = pygame.K_UNKNOWN
    pygame.event.clear()
    while key != pygame.K_q:
        screen.blit(tiles, (0, 0))
        while True:
            event = pygame.event.wait()
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                key =  event.key
                break
        if key == pygame.K_c:
            screen.blit(captions, (0, 0))
        pygame.display.flip()
    pygame.quit()
